libpostfix-parse-mailq-perl (1.005-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 12:48:17 +0100

libpostfix-parse-mailq-perl (1.005-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update GitHub URLs to use HTTPS.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 08:37:54 +0100

libpostfix-parse-mailq-perl (1.005-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 18:09:09 +0100

libpostfix-parse-mailq-perl (1.005-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Import upstream version 1.005
  * Add upstream metadata
  * Update copyright years
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Wed, 12 Aug 2015 19:12:56 +0200

libpostfix-parse-mailq-perl (1.004-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release.

  [ Florian Schlichting ]
  * Import Upstream version 1.004
  * Test::More is in libtest-simple-perl, stupid!
  * Add upstream-repo URL to Source field
  * Bump copyright years
  * Declare compliance with Debian Policy 3.9.5

 -- Florian Schlichting <fsfs@debian.org>  Wed, 29 Jan 2014 22:23:19 +0100

libpostfix-parse-mailq-perl (1.002-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Florian Schlichting ]
  * Import Upstream version 1.002
  * Bump year of upstream copyright
  * Update stand-alone License paragraphs
  * Bump dh compatibility to level 8 (no changes necessary)
  * Add a build-dependency on Test::More 0.96
  * Declare compliance with Debian Policy 3.9.4
  * Switch to source format 3.0 (quilt)
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Fri, 27 Sep 2013 00:06:26 +0200

libpostfix-parse-mailq-perl (1.001-1) unstable; urgency=low

  * Initial Release (Closes: #556886)

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 19 Nov 2009 09:00:57 -0500
